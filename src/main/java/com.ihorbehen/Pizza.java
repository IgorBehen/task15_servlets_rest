package com.ihorbehen;

public class Pizza {
        private static int identifier = 1;
        private String pizzaName;
        private int id;

    public Pizza() {
    }

    public Pizza(String pizzaName) {
        this.pizzaName = pizzaName;
        id = identifier;
        identifier++;
    }

    public int getId() {
            return id;
        }

    public String getPizzaName() {
        return pizzaName;
    }

    @Override
    public String toString() {
        return  "id = " + id  + " - Pizza name = '" + pizzaName + "'";
    }
}




