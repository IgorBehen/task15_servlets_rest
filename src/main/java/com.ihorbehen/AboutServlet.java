package com.ihorbehen;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@WebServlet("/about")
public class AboutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<h4>'The Best Pizza Co.'</h4>");
        out.println("Created by Ihor Behen\n");
        out.println("<P>");
        out.println("Support by EPAM Co.\n");
        out.println("<P>");
        HttpSession session = req.getSession(true);
        Date created = new Date(session.getCreationTime());
        Date accessed = new Date(session.getLastAccessedTime());
        out.println("ID " + session.getId() + "\n");
        out.println("<P>");
        out.println("Created: " + created + "\n");
        out.println("<P>");
        out.println("Last Accessed: " + accessed + "\n");
        out.println("<P>");
        // Echo client's request information
        out.println("Request URI: " + req.getRequestURI() + "\n");
        out.println("<P>");
        out.println("Method: " + req.getMethod() + "\n");
        out.println("<P>");
        out.println("<a href=\'pizzas\'>### Tap to go to the 'PIZZAS' page ###</a>");

    }
}
