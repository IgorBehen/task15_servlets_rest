package com.ihorbehen;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/greetings")
public class GreetingsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<h2>..................................Nice to see you at our site:" +
                ".........................</h2>");
        out.println("<h1>.....................'THE BEST PIZZA'......................</h1>");
        out.println("<h2>You can choose the following types of pizza:</h2>");
        out.println("<h3>- 'PEPPERONI'</h3>");
        out.println("<h3>- 'CHEESE'</h3>");
        out.println("<h3>- 'VEGGIE'</h3>");
        out.println("<h3>- 'CLAM'</h3>");
        out.println("<h3>- 'NEAPOLITAN'</h3>");
        out.println("<h3>- 'MARINARA'</h3>");
        out.println("<h3>- 'CALZONE'</h3>");
        out.println("<h3>- 'MARGARITA'</h3>");
        out.println("<a href=\'pizzas\'>### Tap to go to the 'PIZZAS' page ###</a>");
    }
}