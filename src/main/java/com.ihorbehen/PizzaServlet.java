package com.ihorbehen;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;


public class PizzaServlet extends HttpServlet {
    private static Logger logger1 = LogManager.getLogger(PizzaServlet.class);
    private static  Map<Integer, Pizza> pizzas = new HashMap<>();

    @Override
  public void init() throws ServletException {
      Pizza p1 = new Pizza("NEAPOLITAN");
      Pizza p2 = new Pizza("CALZONE");
      Pizza p3 = new Pizza("MARINARA");
      Pizza p4 = new Pizza("MARGARITA");
      Pizza p5 = new Pizza("CALZONE");
      pizzas.put(p1.getId(), p1);
      pizzas.put(p2.getId(), p2);
      pizzas.put(p3.getId(), p3);
      pizzas.put(p4.getId(), p4);
      pizzas.put(p5.getId(), p5);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/html");
      PrintWriter out = resp.getWriter();
      out.println("<html><head><body>");
      out.println("<p><a href='greetings'>### Tap to return to the 'GREETINGS' page ###</a></p>");
      out.println("<P>");
      out.println("<a href=\'about\'>### Tap to go to the 'ABOUT' page ###</a>");

      out.println("<h2><i>Your pre-orders list: </i></h2>");
      for (Pizza pizza : pizzas.values()) {
          out.println("<p>" + pizza + "</p>");
      }

      out.println("<form>\n" +
              "<p><b>Delete past order by id: </b></p>\n" +
              " <p> Order id: <input type='text' name='pizza_id'>\n" +
              "<form action=\""  +  "pizzas\" " + "method=POST>" +
              "<input type='submit' onclick='remove(this.form.pizza_id.value)' " +
              "name='ok' value='Delete Pizza'/>\n" +
              "</form>"+
              "</form>");

      out.println("<script type='text/javascript'\n>" +
              "   function remove(id) { fetch('pizzas/' + id, {method: 'DELETE'}); }\n" +
              "</script>\n");

      out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$<P>\n");
      out.println("<P>\n");

      out.println("<form action='pizzas' method='POST'>\n" +
              "<h2><b>Make a new order<b>\n" +
              "<P>" +
              "<i>Enter a pizza name: </i><input type='text' name='pizza_name'>" +
              " <button type='submit'>Order Pizza</button>\n" +
              "</h2></form>\n");


      out.println("</body></html>");
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      String newPizzaName = req.getParameter("pizza_name");
      Pizza newPizza = new Pizza(newPizzaName);
      pizzas.put(newPizza.getId(), newPizza);
      doGet(req, resp);

  }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getRequestURI();
        id = id.replace("/task15_Servets_Rest-1.0-SNAPSHOT/pizzas/", "");
        pizzas.remove(Integer.parseInt(id));
    }
  public void destroy() {
      logger1.info("Servlet " + this.getServletName() + " has stopped");
  }
}




















